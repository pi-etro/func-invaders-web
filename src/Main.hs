module Main where

-- libraries
import Graphics.Shine
import Graphics.Shine.Picture
import Graphics.Shine.Image
import System.Random ( newStdGen )
import GHCJS.DOM (currentDocumentUnchecked)

import Controls ( controlEvent )
import World ( createWorld, photographWorld, update, WorldState(Start) )

-- parameters
windowWidth, windowHeight :: Int
windowWidth   = 800
windowHeight  = 600
fps :: Double
fps           = 30

main :: IO ()
main = do
    -- game size configs
    doc     <- currentDocumentUnchecked
    context <- fixedSizeCanvas doc windowWidth windowHeight

    -- load ImageData
    cnon     <- makeImage "https://gitlab.com/pi-etro/func-invaders-web/-/raw/master/resources/cannon.bmp"
    blet     <- makeImage "https://gitlab.com/pi-etro/func-invaders-web/-/raw/master/resources/bullet.bmp"
    oct      <- makeImage "https://gitlab.com/pi-etro/func-invaders-web/-/raw/master/resources/octopus.bmp"
    oct_alt  <- makeImage "https://gitlab.com/pi-etro/func-invaders-web/-/raw/master/resources/octopus_alt.bmp"
    crb      <- makeImage "https://gitlab.com/pi-etro/func-invaders-web/-/raw/master/resources/crab.bmp"
    crb_alt  <- makeImage "https://gitlab.com/pi-etro/func-invaders-web/-/raw/master/resources/crab_alt.bmp"
    sqd      <- makeImage "https://gitlab.com/pi-etro/func-invaders-web/-/raw/master/resources/squid.bmp"
    sqd_alt  <- makeImage "https://gitlab.com/pi-etro/func-invaders-web/-/raw/master/resources/squid_alt.bmp"
    ry       <- makeImage "https://gitlab.com/pi-etro/func-invaders-web/-/raw/master/resources/ray.bmp"
    spcship  <- makeImage "https://gitlab.com/pi-etro/func-invaders-web/-/raw/master/resources/ufo.bmp"
    exp      <- makeImage "https://gitlab.com/pi-etro/func-invaders-web/-/raw/master/resources/alien_explosion.bmp"
    ship_exp <- makeImage "https://gitlab.com/pi-etro/func-invaders-web/-/raw/master/resources/ufo_explosion.bmp"
    bnkr     <- makeImage "https://gitlab.com/pi-etro/func-invaders-web/-/raw/master/resources/bunker.bmp"

    -- sprites
    let cannonBMP      = Rotate pi $ Image Original cnon
    let bulletBMP      = Rotate pi $ Image Original blet
    let octopus        = Rotate pi $ Image Original oct
    let octopus_alt    = Rotate pi $ Image Original oct_alt
    let crab           = Rotate pi $ Image Original crb
    let crab_alt       = Rotate pi $ Image Original crb_alt
    let squid          = Rotate pi $ Image Original sqd
    let squid_alt      = Rotate pi $ Image Original sqd_alt
    let ray            = Rotate pi $ Image Original ry
    let spaceship      = Rotate pi $ Image Original spcship
    let explosion      = Rotate pi $ Image Original exp
    let ship_explosion = Rotate pi $ Image Original ship_exp
    let bunker         = Rotate pi $ Image Original bnkr

    let sprites = [
                    cannonBMP
                  , bulletBMP
                  , octopus
                  , crab
                  , squid
                  , ray
                  , spaceship
                  , bunker
                  , octopus_alt
                  , crab_alt
                  , squid_alt
                  , explosion
                  , ship_explosion
                  ]

    -- random generator
    g <- newStdGen

    -- play game
    play
        context
        doc
        fps
        (createWorld sprites g 0 0 3 Start)
        photographWorld
        controlEvent
        (update sprites g)
