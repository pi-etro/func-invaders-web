module Controls where

-- libraries
import qualified Graphics.Shine.Input as I
import qualified Web.KeyCode as K

import World ( World(shoot, goLeft, goRight) )

-- handle control events
controlEvent :: I.Input -> World a -> World a

-- confirm and fire keys
-- SPACE
controlEvent (I.Keyboard K.Space I.Down _) world      = world { shoot = True }
controlEvent (I.Keyboard K.Space I.Up _) world        = world { shoot = False }
-- LEFT CLICK
controlEvent (I.MouseBtn I.BtnLeft I.Down _) world    = world { shoot = True }
controlEvent (I.MouseBtn I.BtnLeft I.Up _) world      = world { shoot = False }

-- go left keys
-- LEFT ARROW
controlEvent (I.Keyboard K.ArrowLeft I.Down _) world  = world { goRight = True }
controlEvent (I.Keyboard K.ArrowLeft I.Up _) world    = world { goRight = False }
-- A
controlEvent (I.Keyboard K.KeyA I.Down _) world       = world { goRight = True }
controlEvent (I.Keyboard K.KeyA I.Up _) world         = world { goRight = False }

-- go right keys
-- RIGHT ARROW
controlEvent (I.Keyboard K.ArrowRight I.Down _) world = world { goLeft = True }
controlEvent (I.Keyboard K.ArrowRight I.Up _) world   = world { goLeft = False }
-- D
controlEvent (I.Keyboard K.KeyD I.Down _) world       = world { goLeft = True }
controlEvent (I.Keyboard K.KeyD I.Up _) world         = world { goLeft = False }

-- otherwise do nothing
controlEvent _ world = world