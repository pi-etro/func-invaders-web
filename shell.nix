let

  pkgs = import ./config/nixpkgs.nix ;

  web = pkgs.haskell.packages.ghcjs.callCabal2nix "func-invaders-web" ./. {};

in

  {

    web = web.env;

  }
