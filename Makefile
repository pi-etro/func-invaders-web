# build web version with nix-shell -A web
web:
	cabal --builddir=dist-web --config-file=config/web.config build
	mkdir -p static
	ln -sf ../dist-web/build/func-invaders-web/func-invaders-web.jsexe/all.js static/

clean:
	rm -rf dist-* static/all.js
