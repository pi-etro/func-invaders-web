<div align="center">
  <a href="https://gitlab.com/pi-etro/func-invaders-web">
    <img src="img/func_invaders.svg" height="180">
  </a>
</div>
<div align="center">
    <a href="https://www.haskell.org/" alt="Made with Haskell">
        <img src="https://img.shields.io/badge/Made%20with-Haskell-5e5086" /></a>
    <a href="https://www.gnu.org/licenses/gpl-3.0.html" alt="GPLv3">
        <img src="https://img.shields.io/badge/License-GPLv3-CB0000.svg" /></a>
</div>

<br>

<div align="center">
    <a href="https://pi-etro.gitlab.io/func-invaders-web/">Play online now !</a>
</div>

## Controls

To **move** the cannon use `A`/`D` or ⬅️/➡️ keys and to **fire** use `space` or `left click`.

## About

Func Invaders is a Space Invaders clone implemented with the Haskell functional programming language.

This is the web version of the [original project](https://github.com/pi-etro/func-invaders) developed by me for the Programming Paradigms course at UFABC.

Inspired by [Julien Dehos' implementation](https://gitlab.com/juliendehos/haskell-invaders).

Have fun!

## License
[GNU General Public License v3.0](https://www.gnu.org/licenses/gpl-3.0.html)
