let

  pkgs = import ./nixpkgs.nix ;

  func-invaders-web-src = ../.;

  web = pkgs.haskell.packages.ghcjs.callCabal2nix "func-invaders-web" func-invaders-web-src {};

in

  {

    web = pkgs.runCommand "func-invaders-web" { inherit web; } ''
      mkdir -p $out
      ${pkgs.closurecompiler}/bin/closure-compiler ${web}/bin/func-invaders-web.jsexe/all.js > $out/all.js
      cp ${func-invaders-web-src}/static/* $out/
    '';

  }
